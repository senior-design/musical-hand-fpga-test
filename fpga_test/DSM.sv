module DSM (input Clk,
				input SW2,
				input [15:0] Data_in_Sin,
				output [5:0] c,
				output [16:0]ac,
				output Out);
	logic [16:0] accum;
	logic[5:0] count;
	always_ff @ (posedge Clk)
	begin
		if(count==6'd15 || SW2)//oversampled by 4
		begin
			count<=6'b0;
			accum<=accum+Data_in_Sin - {Out,16'b0};//add Sin to upper bits so threshold still works
		end
		else
		begin
			count=count+1'b1;
		end
		
	end
	assign Out = accum[16];//threshold is half of max value for ease of doing this.
	assign c = count;
	assign ac = accum;
	
endmodule
