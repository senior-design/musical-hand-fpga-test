module MC2 (input [7:0] F1, F2, F3,
				input SW2,
				input CLK,
				output[5:0]c,
				output [15:0] dc,
				output PDM_out,
				output[16:0]ac);
	
	logic[7:0] count;
	logic[5:0] count2;
	logic[7:0] pt1;
	logic[7:0] pt2;
	logic[7:0] pt3;
	logic[7:0] inc = 8'd1;
	logic[15:0] o1;
	logic[15:0] o2;
	logic[15:0] o3;
	logic[15:0] S;
	logic[1:0] quad=2'b00;
	logic [16:0] test;
	DSM dsm1(.Clk(CLK), .SW2(SW2), .Data_in_Sin(S), .c(c), .Out(PDM_out), .ac(test));
	SIN_lookup sl1(.in(pt1), .Out(o1));
	SIN_lookup sl2(.in(pt2), .Out(o2));
	SIN_lookup sl3(.in(pt3), .Out(o3));
	always_ff @ (posedge CLK)
	begin
		if(count2==6'd15)
		begin
			if(count==F1[7:1]+8'd26)
				begin
					count<=8'b0;
					if(pt1==8'hFF&&quad==2'b00)
					begin
						quad=2'b01;
						inc=8'hFFFF;
					end
					else if(pt1==8'h00&&quad==2'b01)
					begin
						quad=2'b10;
						inc=8'h01;
					end
					else if(pt1==8'hFF&&quad==2'b10)
					begin
						quad=2'b11;
						inc=8'hFF;
					end
					else if(pt1==8'h00&&quad==2'b11)
					begin
						quad=2'b00;
						inc=8'h01;
					end
					pt1<=pt1+inc;
					pt2<=pt2+inc;
					pt3<=pt3+inc;
					if(quad[1]==1'b1)
					begin
						S<=o1+16'd7500;
					end
					else
					begin
						S<=16'd7500-o1;
					end
					//S<=(~o1)+16'd1500;
				end
				else
				begin
					count=count+1'b1;
				end
		end
		else
		begin
			count2=count2+1'b1;
		end
		
	end
	assign dc=o1;
	assign ac=test;
	
endmodule
