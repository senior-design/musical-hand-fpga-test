module MC1 (input logic [11:0] a0, a1, a2,
				input logic CLK,
				output logic [7:0] f0,f1,f2);

	assign f0 = a0[11:4];
	assign f1 = a1[11:4];
	assign f2 = a2[11:4];
endmodule
